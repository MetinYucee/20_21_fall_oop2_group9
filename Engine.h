#include"Tank.h"

class Engine
{
private:
	friend class Simulation;
	static double fuel_per_second;
	bool status;
	bool connect;
	int count;
public:
	Engine();
	~Engine();
	void start_engine();
	void stop_engine();
	void connect_fuel_tank_to_engine(std::string tank_id);
	void disconnect_fuel_tank_from_engine(std::string tank_id);
	void list_connected_tanks();
};
