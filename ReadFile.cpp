#include"ReadFile.h"
#include "Engine.h"
#include "Tank.h"
#include"Simulationh.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

void ReadFile::read_file(char* fileName) {
	ifstream file;
	string line;

	file.open(fileName);
	if (file.is_open()) {
		cout << fileName << " file is opened." << endl;
		int i = 0;
		while (getline(file, line)) {
			if (i == size - 1)
			{
				this->size = this->size * 2;
				string* tempArr = new string[this->size];
				for (int j = 0; j < size / 2; j++) tempArr[j] = this->commandArr[j];
				delete[] this->commandArr;
				this->commandArr = tempArr;
			}
			commandArr[i] = line;
			i++;
		}
		file.close();
	}
	else
		cout << fileName << " File cannot open!" << endl;
}
void ReadFile::print_to_file(char* fileName) {
	ofstream file(fileName);
	

	if (file.is_open()) {
		cout << fileName << " file is opened." << endl;
	}
	else {
		cout << fileName << " file cannot opened." << endl;
	}

	file.close();
}

void ReadFile::get_commands() {

	Tank tank;
	Simulation s;
	Engine engine;

		string command, tempCommand, tempParameter;
		int j = 0;
		for (int i = 0; i < size; i++)
		{
			command = commandArr[i];
			while (j < command.length() && command[j] != '<' && command[j] != ';' && command[j] != ' ')
			{
				tempCommand += command[j];
				j++;
			}
			j = j + 2;
			while (j < command.length() && command[j] != '>' && command[j] != ';')
			{
				tempParameter += command[j];
				j++;
			}


			if (tempCommand == "start_engine")
			{
				engine.start_engine();
			}

			else if (tempCommand == "give_back_fuel")
			{
				double parameter = stod(tempParameter);
				tank.give_back_fuel(parameter);
			}
			else if (tempCommand == "add_fuel_tank")
			{
				double parameter = stod(tempParameter);
				tank.add_fuel_tank();
			}
			else if (tempCommand == "list_fuel_tanks")
			{
				tank.list_fuel_tanks();
			}
			else if (tempCommand == "remove_fuel_tank")
			{

				tank.remove_fuel_tank(tempParameter);
			}
			else if (tempCommand == "connect_fuel_tank_to_engine")
			{

				engine.connect_fuel_tank_to_engine(tempParameter);
			}
			else if (tempCommand == "stop_engine")
			{
				engine.stop_engine();
			}
			else if (tempCommand == "disconnect_fuel_tank_from_engine")
			{

				engine.disconnect_fuel_tank_from_engine(tempParameter);
			}
			else if (tempCommand == "open_valve")
			{

				tank.open_valve(tempParameter);
			}
			else if (tempCommand == "close_valve")
			{

				tank.close_valve(tempParameter);
			}
			else if (tempCommand == "break_fuel_tank")
			{

				tank.break_fuel_tank(tempParameter);
			}
			else if (tempCommand == "repair_fuel_tank")
			{

				tank.repair_fuel_tank(tempParameter);
			}
			else if (tempCommand == "stop_simulation")
			{
				s.stop_simulation();
			}
			else { cout << "Invalid command!" << endl; }
			tempCommand = "";
			tempParameter = "";
			j = 0;
		}
}