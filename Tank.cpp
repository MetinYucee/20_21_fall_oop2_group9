#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "Tank.h"

using namespace std;

int Tank::ID = 0;

Tank::Tank()
{

}

Tank::Tank(double capacity)
{
	this->capacity = capacity;
	this->fuelQuantity = 0;
	this->tankID = to_string(Tank::ID);
	Tank::ID++;
	this->valve = false;
}

Tank::~Tank() {

}

void Tank::RepairTank() {
	this->broken = true;

}

void Tank::BreakTank() {

	this->broken = false;
}

void Tank::OpenValve() {
	this->valve = true;
}

void Tank::CloseValve() {
	this->valve = false;
}

bool Tank::IsBroken() {
	if (this->broken)
		return 1;
	else
		return 0;
}

void Tank::SetTankCapacity(double capacity) {
	this->capacity = capacity;
}

double Tank::GetTankCapacity() const {
	return this->capacity;
}

void Tank::SetFuelQuantity(double fuelQuantity) {
	this->fuelQuantity = fuelQuantity;
}

double Tank::GetFuelQuantity() const {
	return this->fuelQuantity;
}

string Tank::GetTankID() const {
	return this->tankID;
}

void Tank::SetConnectedStatus(string status) {
	this->connected = status;
}

string Tank::GetConnectedStatus() const {
	return this->connected;
}

bool Tank::IsOpenedValve()
{
	return this->valve;
}
