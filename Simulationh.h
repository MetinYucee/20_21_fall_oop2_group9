#ifndef SIMULATION_H
#define SIMULATION_H

class Simulation {
private:
	int seconds;
public:
	Simulation();
	~Simulation();
	void stop_simulation();
	void wait(int seconds);
};
#endif