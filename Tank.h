#pragma once
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
using namespace std;
class Tank {
private:
	friend class ReadFile;
	double capacity;
	bool broken;
	string connected;
	double fuelQuantity;
	friend class Simulation;
	bool valve;
	string tankID;
	static int ID;
public:
	Tank();
	Tank(double capacity);
	
	void OpenValve();
	bool IsOpenedValve();
	void SetTankCapacity(double);
	void BreakTank();
	void RepairTank();
	double GetTankCapacity() const;
	void CloseValve();
	string GetTankID() const;
	bool IsBroken();
	void SetFuelQuantity(double);
	double GetFuelQuantity() const;
	void SetConnectedStatus(string);
	string GetConnectedStatus() const;



}Tank;
