#include <string>

class ReadFile {
private:
	friend class Engine;
	std::string* commandArr;
	size_t size = 20;

public:
	ReadFile();
	~ReadFile();
	void read_file(char* fileName);
	void print_to_file(char* fileName);
	void get_commands();
	void stop_simulation();






};